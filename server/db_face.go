package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"math/rand"
)

var db *sql.DB

const dbaddr = "guest:12345678@tcp(127.0.0.1)/glack"

func init() {
	var err error
	db, err = sql.Open("mysql", dbaddr)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	err = db.Ping()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS users (id int AUTO_INCREMENT PRIMARY KEY, " +
		"handle varchar(255) UNIQUE NOT NULL, " +
		"password varchar(255), salt varchar(255), username varchar(255), last_online DATETIME)")
	if err != nil {
		log.Printf(err.Error())
		return
	}

	// create per-user tables
	rows, err := db.Query("SELECT id FROM users")
	if err != nil {
		log.Printf(err.Error())
		return
	}
	var id int64
	for rows.Next() {
		rows.Scan(&id)
		createUserTables(id)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS chats (id INT AUTO_INCREMENT PRIMARY KEY, multi BOOLEAN NOT NULL, name varchar(255))")
	if err != nil {
		log.Printf(err.Error())
		return
	}

	// create per-chat tables
	chatrows, err := db.Query("SELECT id FROM chats")
	if err != nil {
		log.Printf(err.Error())
		return
	}
	var chatid int64
	for chatrows.Next() {
		chatrows.Scan(&chatid)
		createChatTables(chatid)

	}
}

func addToContactList(adder_handle, addee_handle string) error {
	adder_id, err := getIDByHandle(adder_handle)
	if err != nil {
		return errors.New("no such user")
	}
	addee_id, err := getIDByHandle(addee_handle)
	if err != nil {
		return errors.New("no such user")
	}
	_, err = db.Exec(fmt.Sprintf("INSERT INTO user_%d_contacts (contact_id) VALUES (%d)", adder_id, addee_id))
	return err
}

func isValidUser(name string) (bool, error) {
	if len(name) == 0 {
		return false, errors.New("No name supplied")
	}
	rows, err := db.Query(fmt.Sprintf("SELECT COUNT(*) FROM users WHERE handle=\"%s\"", name))
	if err != nil {
		log.Printf(err.Error())
		return false, err
	}
	cnt := 0
	for rows.Next() {
		rows.Scan(&cnt)
	}
	return cnt > 0, nil
}

func createUserTables(id int64) {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS user_%d_contacts (id int AUTO_INCREMENT PRIMARY KEY,"+
		" contact_id int UNIQUE NOT NULL)", id))
	if err != nil {
		log.Printf(err.Error())
		return
	}
	_, err = db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS user_%d_chats (id int AUTO_INCREMENT PRIMARY KEY, "+
		"chat_id int UNIQUE NOT NULL)", id))
	if err != nil {
		log.Printf(err.Error())
		return
	}
}

func createChatTables(id int64) {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS chat_%d_users (id INT AUTO_INCREMENT PRIMARY KEY, " +
		"user_id int UNIQUE)", id))
	if err != nil {
		log.Printf(err.Error())
		return
	}
	_, err = db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS chat_%d_messages (id INT AUTO_INCREMENT PRIMARY KEY, " +
		"sent_by INT, sent_on DATETIME, text VARCHAR(1000), received BOOLEAN)", id))
	if err != nil {
		log.Printf(err.Error())
		return
	}
}

func addNewUser(handle, password string) error {
	c := make([]byte, 10)
	rand.Read(c)
	salt := hex.EncodeToString(c)
	phash := password + salt
	h := sha256.New()
	bhash, _ := hex.DecodeString(phash)
	h.Write(bhash)
	resp, err := db.Exec(fmt.Sprintf("INSERT INTO users (handle, password, salt) VALUES (\"%s\", \"%s\", \"%s\") ",
		handle, hex.EncodeToString(h.Sum(nil)), salt))
	if err != nil {
		return err
	}
	id, _ := resp.LastInsertId()
	createUserTables(id)
	return err
}

func checkPassword(handle, password string) bool {
	rows, err := db.Query(fmt.Sprintf("SELECT password, salt FROM users WHERE handle=\"%s\"", handle))
	if err != nil {
		return false
	}
	var pwd, salt string
	for rows.Next() {
		rows.Scan(&pwd, &salt)
		phash := password + salt
		h := sha256.New()
		bhash, _ := hex.DecodeString(phash)
		h.Write(bhash)
		result := hex.EncodeToString(h.Sum(nil))
		if result == pwd {
			return true
		} else {
			return false
		}
	}
	return false
}

func getIDByHandle(handle string) (int32, error) {
	rows, err := db.Query(fmt.Sprintf("SELECT id FROM users WHERE handle=\"%s\"", handle))
	if err != nil {
		return 0, err
	}
	var id int32
	for rows.Next() {
		rows.Scan(&id)
		return id, nil
	}
	return 0, errors.New("no such user")
}

func getHandleByID(id int64) (string, error) {
	rows, err := db.Query(fmt.Sprintf("SELECT handle FROM users WHERE id=%d", id))
	if err != nil {
		return "", err
	}
	var h string
	for rows.Next() {
		rows.Scan(&h)
		return h, nil
	}
	return "", errors.New("no such user")
}

func addMessage(chatId int32, from string, sentOn int64, text string) error {
	id, err := getIDByHandle(from)
	if err != nil {
		return err
	}
	_, err = db.Exec(fmt.Sprintf("INSERT INTO chat_%d_messages (from, sent_on, text, received) VALUES "+
		"(\"%d\",FROM_UNIXTIME(%d), \"%s\")"), id, sentOn, text)
	return err
}

// checks if user with userID already has a (personal) chat with chatWithID
func checkUserHasChat(userID int32, chatWithID int32) (bool, error) {
	rows, err := db.Query(fmt.Sprintf("SELECT chat_id FROM user_%d_chats", userID))
	if err != nil {
		log.Printf(err.Error())
		return false, err
	}
	var chatID int64
	var multi bool
	for rows.Next() {
		rows.Scan(&chatID)
		chatrows, _ := db.Query(fmt.Sprintf("SELECT multi FROM chats WHERE chat_id=%d"), chatID) // we dont need a multichat (for now)
		for chatrows.Next() {
			chatrows.Scan(&multi)
			if !multi {
				userrows, err := db.Query(fmt.Sprintf("SELECT COUNT(*) FROM chat_%d_users WHERE user_id=%d"), chatWithID) // we dont need a multichat (for now)
				if err != nil {
					log.Printf(err.Error())
					return false, err
				}
				cnt := 0
				for userrows.Next() {
					userrows.Scan(&cnt)
				}
				if cnt > 0 {
					return true, nil
				}
			}
		}
	}
	return false, nil
}
