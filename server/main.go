package main

import (
	gp "../glackproto"
	"context"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/ptypes/timestamp"
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"net"
	"strconv"
	"time"
)

const cookieLength = 32

var (
	port = flag.Int("port", 1488, "port to listen to")
)

func init() {
	rand.Seed(time.Now().Unix())
	log.SetFlags(log.Lshortfile)
}

type glackServer struct {
	wait     chan interface{}
	sessions map[string]string
}

func (srv *glackServer) Register(ctx context.Context, req *gp.RegisterRequest) (*gp.RegisterResponse, error) {
	v, err := isValidUser(req.GetUsername())
	if err != nil {
		return &gp.RegisterResponse{
			Success:  false,
			Response: "internal server error",
		}, nil
	}
	if v {
		return &gp.RegisterResponse{
			Success:  false,
			Response: "user already exists",
		}, nil
	}
	err = addNewUser(req.GetUsername(), req.GetPassword())
	if err != nil {
		log.Println(err.Error())
		return &gp.RegisterResponse{
			Success:  false,
			Response: "failed to register user",
		}, nil
	} else {
		return &gp.RegisterResponse{
			Success:  true,
			Response: fmt.Sprintf("you may now use %s handle", req.GetUsername()),
		}, nil
	}
}

func (srv *glackServer) Login(ctx context.Context, req *gp.LoginRequest) (*gp.LoginResponse, error) {
	log.Printf("Login %s:%s", req.Username, req.Password)
	if res, _ := isValidUser(req.GetUsername()); !res { // user not found
		log.Printf("user not found")
		return &gp.LoginResponse{
			Success: false,
		}, nil
	}
	if !checkPassword(req.GetUsername(), req.GetPassword()) {
		return &gp.LoginResponse{
			Success: false,
		}, nil
	}
	srv.sessions[req.Username] = generateCookie()
	return &gp.LoginResponse{
		Success: true,
		Cookie:  srv.sessions[req.Username],
	}, nil
}

func (srv *glackServer) SendMessage(ctx context.Context, req *gp.SendMessageRequest) (*gp.SendMessageResponse, error) {
	log.Printf("SendMessage %q: %s->%s: %s", req.SentOn, req.From, req.Text)
	if srv.sessions[req.From] != req.Cookie {
		return &gp.SendMessageResponse{Ok: false, Error: "wrong cookie"}, nil
	}
	err := addMessage(req.ChatId, req.From, req.SentOn.Seconds, req.Text)
	if err != nil {
		log.Printf(err.Error())
		return nil, err //possible crash?
	}
	return &gp.SendMessageResponse{
		Ok: true,
		Sent: &timestamp.Timestamp{
			Seconds: time.Now().Unix(),
		},
	}, nil
}

func (srv *glackServer) GetMessages(req *gp.GetMessagesRequest, stream gp.Glack_GetMessagesServer) error {
	if srv.sessions[req.MyHandle] != req.Cookie {
		return errors.New("wrong cookie") // what happens then?
	}
	rows, err := db.Query(fmt.Sprintf("SELECT * from chat_%d_messages WHERE sent_on > FROM_UNIXTIME(%d)", req.ChatId, req.Since))
	if err != nil {
		log.Printf(err.Error())
		return err
	}
	var (
		from     string
		text     string
		sent_on  mysql.NullTime
		received bool
	)
	for rows.Next() {
		if err := rows.Scan(&from, &sent_on, &text, &received); err != nil {
			log.Printf(err.Error())
			return err
		} else {
			err = stream.Send(&gp.GlackMessage{
				From:   from,
				ChatId: req.ChatId,
				Text:   text,
				SentOn: &timestamp.Timestamp{Seconds: sent_on.Time.Unix()}})
			if err != nil {
				log.Printf(err.Error())
				return err
			}
		}
	}
	return nil
}

func (srv *glackServer) GetContactList(ctx context.Context, req *gp.GetContactListRequest) (*gp.ContactList, error) {
	if srv.sessions[req.Handle] != req.Cookie {
		return &gp.ContactList{Ok: false, Error: "wrong cookie"}, nil
	}
	userID, err := getIDByHandle(req.Handle)
	if err != nil {
		log.Printf(err.Error())
		return &gp.ContactList{Ok: false, Error: "internal server error"}, nil
	}
	rows, err := db.Query(fmt.Sprintf("SELECT contact_id FROM user_%d_contacts", userID))
	if err != nil {
		log.Printf(err.Error())
		return &gp.ContactList{Ok: false, Error: "internal server error"}, nil
	}
	var h string
	var id int64
	var cl = new(gp.ContactList)
	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return cl, err
		} else {
			h, _ = getHandleByID(id)
			cl.Handles = append(cl.Handles, h)
			cl.Ids = append(cl.Ids, int32(id))
		}
	}
	cl.Ok = true
	return cl, nil
}

func (srv *glackServer) AddToContactList(ctx context.Context, req *gp.AddToContactListRequest) (*gp.AddToContactListResponse, error) {
	if srv.sessions[req.AdderHandle] != req.Cookie {
		return &gp.AddToContactListResponse{Ok: false, Error: "wrong cookie"}, nil
	}

	err := addToContactList(req.AdderHandle, req.HandleToAdd)
	if err != nil {
		log.Printf(err.Error())
		return &gp.AddToContactListResponse{Ok: false, Error: err.Error()}, nil
	}
	return &gp.AddToContactListResponse{Ok: true, Error: ""}, nil
}

func (srv *glackServer) GetChatList(req *gp.GetChatListRequest, stream gp.Glack_GetChatListServer) error {
	if srv.sessions[req.Handle] != req.Cookie {
		return errors.New("wrong cookie")
	}
	id, err := getIDByHandle(req.Handle)
	if err != nil {
		return err
	}
	rows, err := db.Query(fmt.Sprintf("SELECT chat_id FROM user_%d_chats", id))
	if err != nil {
		return err
	}
	var (
		chat_id int32
		multi   bool
		name    string
	)
	for rows.Next() {
		rows.Scan(&chat_id)
		chatrow, err := db.Query(fmt.Sprintf("SELECT multi, name FROM chats WHERE id=%d", chat_id))
		if err != nil {
			return err
		}
		for chatrow.Next() {
			chatrow.Scan(&multi, &name)
			stream.Send(&gp.Chat{Id: chat_id, Multi: multi, Handle: name})
		}
	}
	return nil
}

func (srv *glackServer) CreateChat(ctx context.Context, req *gp.CreateChatRequest) (*gp.CreateChatResponse, error) {
	if srv.sessions[req.CreatorHandle] != req.Cookie {
		return &gp.CreateChatResponse{Ok: false, Error: "wrong cookie"}, nil
	}
	creatorID, err := getIDByHandle(req.CreatorHandle)
	if err != nil {
		return &gp.CreateChatResponse{
			Ok:    false,
			Error: "No such user"}, nil
	}
	has, err := checkUserHasChat(creatorID, req.VisavisId)
	if !has {
		res, _ := db.Exec(fmt.Sprintf("INSERT INTO chats (multi) VALUES (false)"))
		chatid, _ := res.LastInsertId()
		createChatTables(chatid)
		db.Exec(fmt.Sprintf("INSERT INTO user_%d_chats (chat_id) VALUES (%d)", creatorID, chatid))
		db.Exec(fmt.Sprintf("INSERT INTO user_%d_chats (chat_id) VALUES (%d)", req.VisavisId, chatid))
	}
	return &gp.CreateChatResponse{
		Ok:    true,
		Error: ""}, nil
}

func generateCookie() string {
	c := make([]byte, cookieLength)
	rand.Read(c)
	return hex.EncodeToString(c)
}

func main() {
	flag.Parse()
	//var waitch = make(chan interface{})
	listener, err := net.Listen("tcp", ":"+strconv.Itoa(*port))
	if err != nil {
		log.Printf(err.Error())
	}
	grpcServer := grpc.NewServer()
	gp.RegisterGlackServer(grpcServer, &glackServer{sessions: make(map[string]string)})
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Println(err.Error())
	}
	//<-waitch
}
