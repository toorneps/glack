package main

import (
	gp "../glackproto"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"flag"
	"github.com/golang/protobuf/ptypes/timestamp"
	"google.golang.org/grpc"
	"io"
	"log"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var (
	serverAddr = flag.String("s", "localhost:1488", "server address (host:port)")
	handle     = flag.String("u", "", "user handle")
	password   = flag.String("p", "", "user password")
	newUser    = flag.Bool("r", false, "register new user")
)

type glack struct {
	exit       chan struct{}
	grpcClient gp.GlackClient
	cookie     string

	tui glackTUI
}

func (c *glack) fetchMessages(chat_id int) {
	for {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		stream, err := c.grpcClient.GetMessages(ctx, &gp.GetMessagesRequest{
			MyHandle: *handle,
			Cookie:   c.cookie,
			ChatId:   0,
			Since:    nil,
		})
		if err != nil {
			return // TODO
		}
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				break
			}
			c.tui.sessions[chat_id].append(resp)
		}
		c.tui.redraw()
		time.Sleep(time.Second)
	}
}

func (c *glack) fetchChats() {
	for {
		_ = c.getChats()
		time.Sleep(time.Millisecond * 500)
	}
}

func (c *glack) getContacts() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cl, err := c.grpcClient.GetContactList(ctx, &gp.GetContactListRequest{
		Handle: *handle,
		Cookie: c.cookie,
	})
	if err != nil {
		return err
	}
	if cl.Ok == false {
		return errors.New("could not get contact list")
	}

	for _, cnt := range cl.Handles {
		c.tui.contacts = append(c.tui.contacts, &contact{handle: cnt})
	}
	c.tui.redraw()
	return nil
}

func (c *glack) getChats() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	c.tui.chatsMtx.Lock()
	defer c.tui.chatsMtx.Unlock()
	c.tui.chats = nil // clear chat list
	cl, err := c.grpcClient.GetChatList(ctx, &gp.GetChatListRequest{Handle: *handle, Cookie: c.cookie})
	if err != nil {
		return err
	}
	for {
		chat, err := cl.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		c.tui.chats = append(c.tui.chats, &chatDesc{handle: chat.Handle, id: int(chat.Id)})
	}
	return nil
}

func (c *glack) login() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	h := sha256.New()
	h.Write([]byte(*password))
	phash := hex.EncodeToString(h.Sum(nil))
	if *newUser {
		c.tui.printf("Registering new user...")
		logResp, err := c.grpcClient.Register(ctx, &gp.RegisterRequest{
			Username: *handle,
			Password: phash,
		})
		if err != nil {
			log.Printf(err.Error())
			return err
		} else {
			c.tui.printf(" success!\n")
			log.Println(logResp.GetResponse())
		}
	}
	c.tui.printf("Logging in...")
	logResp, err := c.grpcClient.Login(ctx, &gp.LoginRequest{
		Username: *handle,
		Password: phash,
	})
	if err != nil {
		return err
	} else {
		if logResp.Success {
			c.tui.printf(" success!")
			c.cookie = logResp.Cookie
			return nil
		} else {
			return errors.New("invalid login/passwd")
		}
	}
}

func (c *glack) sendMessage(message *outgoingMessage) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	c.grpcClient.SendMessage(ctx, &gp.SendMessageRequest{
		Cookie: c.cookie,
		From:   *handle,
		ChatId: int32(message.id),
		Text:   message.text,
		SentOn: &timestamp.Timestamp{Seconds: int64(time.Now().Unix())},
	})
	return nil
}

func (c *glack) addToContacts(h string){
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	resp, err := c.grpcClient.AddToContactList(ctx, &gp.AddToContactListRequest{
		AdderHandle: *handle,
		HandleToAdd: h,
		Cookie:      c.cookie})
	if err != nil {
		log.Printf(err.Error())
	}
	if resp.Ok {
		go c.getContacts()
	}
}

func (c *glack) createChat(id int){
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	resp, err := c.grpcClient.CreateChat(ctx, &gp.CreateChatRequest{
		CreatorHandle: *handle,
		VisavisId:     int32(id),
		Cookie:        c.cookie,
	})
	if err != nil {
		log.Printf(err.Error())
	}
	if resp.Ok {
		go c.getChats()
	}
}

func (c *glack) processCommand(s string) {
	tokens := strings.Split(s, " ")
	cmd := tokens[0]
	switch cmd {
	case "add":
		{
			if len(tokens) == 1 {
				return
			}
			handleToAdd := tokens[1]
			go c.addToContacts(handleToAdd)
		}
	case "create_chat":
		{
			if len(tokens) == 1 {
				return
			}
			id, _ := strconv.Atoi(tokens[1])
			go c.createChat(id)
		}
	}
}

func (c *glack) work() error {
	for {
		select {
		case <-c.tui.exit:
			close(c.exit) // finish the program
		case m := <-c.tui.outMessages:
			go c.sendMessage(m)
		case cmd := <-c.tui.commands:
			go c.processCommand(cmd)
		}
	}
}

func (c *glack) setup() error {
	conn, err := grpc.Dial(*serverAddr, grpc.WithInsecure())
	if err != nil {
		log.Printf(err.Error())
		return err
	}
	c.grpcClient = gp.NewGlackClient(conn)

	c.exit = make(chan struct{})
	c.tui.setup()
	return nil
}

func main() {
	flag.Parse()
	var client glack
	defer exec.Command("reset").Run()
	if err := client.setup(); err != nil {
		log.Printf(err.Error())
		return
	}
	defer client.tui.cleanup()

	if err := client.login(); err != nil {
		log.Printf(err.Error())
		return
	}

	if err := client.getContacts(); err != nil {
		log.Printf(err.Error())
		return
	}

	if err := client.getChats(); err != nil {
		log.Printf(err.Error())
		return
	}
	go client.tui.work()
	for _, h := range client.tui.chats {
		client.tui.sessions[h.id] = *NewSession()
		go client.fetchMessages(h.id)
	}
	go client.fetchChats()
	go client.work()

	<-client.exit
}
