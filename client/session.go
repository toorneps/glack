package main

import (
	gp "../glackproto"
	"fmt"
	"sort"
	"sync"
	"time"
)

type chatContext []*gp.GlackMessage

func (cc chatContext) Len() int {
	return len(cc)
}

func (cc chatContext) Swap(i, j int) {
	cc[i], cc[j] = cc[j], cc[i]
}

func (cc chatContext) Less(i, j int) bool {
	return cc[i].SentOn.Seconds < cc[j].SentOn.Seconds
}

func formatMessage(message *gp.GlackMessage) string {
	t := time.Unix(message.SentOn.Seconds, int64(message.SentOn.Nanos))
	return fmt.Sprintf("%v  %s: %s", t, message.From, message.Text)
}

type session struct {
	chatMtx sync.RWMutex
	chatCtx chatContext
}

func (s session) append(m *gp.GlackMessage) {
	s.chatMtx.Lock()
	s.chatCtx = append(s.chatCtx, m)
	s.chatMtx.Unlock()
}

func (s session) sort() {
	s.chatMtx.Lock()
	if !sort.IsSorted(s.chatCtx) {
		sort.Sort(s.chatCtx)
	}
	s.chatMtx.Unlock()
}

func NewSession() *session {
	var s session
	s.chatCtx = make([]*gp.GlackMessage, 0, 100)
	return &s
}

func (s session) getLast(n int) []string {
	s.chatMtx.RLock()
	if n > len(s.chatCtx) {
		n = len(s.chatCtx)
	}
	buf := make([]string, n, n)
	for i := n - 1; i >= 0; i-- {
		buf[0] = fmt.Sprintf("%s", formatMessage(s.chatCtx[-i]))
	}
	s.chatMtx.RUnlock()
	return buf
}
