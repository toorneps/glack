package main

import (
	"fmt"
	"github.com/nsf/termbox-go"
	"sync"
)

const coldef = termbox.ColorDefault

type Mode int

const (
	chatMode Mode = iota
	contactListMode
	chatListMode
	commandMode
	numberOfModes // to cycle
)

type chatDesc struct {
	handle string
	id     int
}

type contact struct {
	handle string
	id     int
}
type outgoingMessage struct {
	text string
	id   int
}
type glackTUI struct {
	currentChatId int
	mode          Mode

	inputCtx   string
	commandCtx string
	sessions   map[int]session

	contacts []*contact
	chats    []*chatDesc
	chatsMtx sync.RWMutex

	cursorPos int

	outMessages chan *outgoingMessage
	commands    chan string
	exit        chan struct{}

	mtx sync.Mutex
}

func (gt * glackTUI) getChats() []*chatDesc{
	gt.chatsMtx.RLock()
	defer gt.chatsMtx.RUnlock()
	return gt.chats
}

func (gt *glackTUI) work() {
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			switch ev.Key {
			case termbox.KeyEnter:
				switch gt.mode {
				case chatMode:
					{
						gt.outMessages <- &outgoingMessage{
							text: gt.inputCtx,
							id:   gt.currentChatId,
						}
						gt.inputCtx = ""
					}
				case contactListMode:
					{
						gt.currentChatId = gt.contacts[gt.cursorPos].id
						gt.mode = chatMode
						gt.commands <- fmt.Sprintf("create_chat %d", gt.currentChatId)
					}
				case chatListMode:
					{
						gt.currentChatId = gt.getChats()[gt.cursorPos].id
						gt.mode = chatMode
					}
				case commandMode:
					{
						gt.commands <- gt.commandCtx
						gt.commandCtx = ""
					}
				}
			case termbox.KeyCtrlC:
				gt.exit <- struct{}{}
				return
			case termbox.KeyEsc:
				gt.mode = (gt.mode + 1) % numberOfModes
			case termbox.KeyBackspace:
				fallthrough
			case termbox.KeyBackspace2:
				switch gt.mode {
				case chatMode:
					if len(gt.inputCtx) > 0 {
						gt.inputCtx = gt.inputCtx[:len(gt.inputCtx)-1]
					}
				case commandMode:
					if len(gt.commandCtx) > 0 {
						gt.commandCtx = gt.commandCtx[:len(gt.commandCtx)-1]
					}
				}
			case termbox.KeyArrowUp:
				if gt.mode != chatMode && gt.cursorPos > 0 {
					gt.cursorPos--
				}
			case termbox.KeyArrowDown:
				if gt.mode != chatMode && gt.cursorPos < len(gt.contacts) {
					gt.cursorPos++
				}
			case termbox.KeySpace: // space is a special key, but we need the character
				ev.Ch = ' '
				fallthrough
			default:
				switch gt.mode {
				case chatMode:
					{
						gt.inputCtx = gt.inputCtx + string(ev.Ch)
					}
				case commandMode:
					{
						gt.commandCtx = gt.commandCtx + string(ev.Ch)
					}
				}
			}
			gt.redraw()
		case termbox.EventError:
			gt.exit <- struct{}{}
		}
	}
}

func (gt *glackTUI) redraw() {
	gt.mtx.Lock()
	_ = termbox.Clear(coldef, coldef)
	_ = termbox.Flush()
	_, height := termbox.Size()

	switch gt.mode {
	case chatMode:
		{
			if len(gt.getChats()) == 0{
				break
			}
			linesNum := height - 2
			gt.sessions[gt.currentChatId].sort()
			lines := gt.sessions[gt.currentChatId].getLast(linesNum)
			if len(lines) < linesNum {
				linesNum = len(lines)
			}
			tbprint(0, 0, coldef, termbox.ColorCyan, fmt.Sprintf("chat with %s", gt.getChats()[gt.currentChatId].handle))
			for i := 0; i < linesNum; i++ {
				tbprint(0, 1+i, coldef, coldef, fmt.Sprintf(">%s", lines[i]))
			}
			tbprint(0, height-1, coldef, coldef, fmt.Sprintf(">%s", gt.inputCtx))
		}
	case contactListMode:
		{
			linesNum := height - 1
			tbprint(0, 0, termbox.ColorRed, coldef, "contact list:")
			if linesNum > len(gt.contacts) {
				linesNum = len(gt.contacts)
			}
			// TODO list scrolling
			for i := 0; i < linesNum; i++ {
				if i == gt.cursorPos {
					tbprint(0, 1+i, termbox.ColorBlack, termbox.ColorGreen, gt.contacts[i].handle)
				} else {
					tbprint(0, 1+i, coldef, coldef, gt.contacts[i].handle)
				}
			}
		}
	case chatListMode:
		{
			linesNum := height - 1
			tbprint(0, 0, termbox.ColorRed, coldef, "chat list:")
			if linesNum > len(gt.getChats()) {
				linesNum = len(gt.getChats())
			}

			// TODO list scrolling
			for i := 0; i < linesNum; i++ {
				if i == gt.cursorPos {
					tbprint(0, 1+i, termbox.ColorBlack, termbox.ColorGreen, gt.getChats()[i].handle) // todo if multi then handle, else?
				} else {
					tbprint(0, 1+i, coldef, coldef, gt.getChats()[i].handle)
				}
			}
		}
	case commandMode:
		{
			tbprint(0, 0, termbox.ColorRed, coldef, "command mode")
			tbprint(0, height-1, coldef, coldef, fmt.Sprintf(">%s", gt.commandCtx))
		}
	}

	_ = termbox.Flush()
	gt.mtx.Unlock()
}

func tbprint(x, y int, fg, bg termbox.Attribute, msg string) {
	for _, c := range msg {
		termbox.SetCell(x, y, c, fg, bg)
		x++
	}
}

func (gt *glackTUI) setup() {
	gt.mode = chatListMode
	err := termbox.Init()
	if err != nil {
		panic(err)
	}
	termbox.SetInputMode(termbox.InputEsc)
	gt.exit = make(chan struct{})
	gt.outMessages = make(chan *outgoingMessage)
	gt.commands = make(chan string)
	gt.contacts = make([]*contact, 0, 10)
	gt.sessions = make(map[int]session)
	gt.redraw()
}

func (gt *glackTUI) cleanup() {
	termbox.Close()
}

func (gt *glackTUI) printf(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a)
	i := 0
	for _, ch := range s {
		tbprint(i, 0, coldef, coldef, string(ch))
		i++
	}
}
