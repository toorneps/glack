export GOPATH="/home/user/go"
export PATH=$GOPATH/bin:$PATH
SRC_DIR=~/lab/glack/glackproto
DST_DIR=~/lab/glack/glackproto
INC_DIR=/usr/local/include
protoc -I=$SRC_DIR -I $INC_DIR $SRC_DIR/glackproto.proto --go_out=plugins=grpc:glackproto

#protoc -I routeguide/ routeguide/route_guide.proto --go_out=plugins=grpc:routeguide
